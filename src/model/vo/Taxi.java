package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{
	private String taxi_id;
	private String company;
	private DoubleLinkedList<Service> servicios;
	/**
	 * contiene un taxi
	 * @param taxi_id
	 * @param company
	 */
	public Taxi(String ptaxi_id,String pcompany)
	{
		taxi_id = ptaxi_id;
		company = pcompany;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * add a service to a taxi
	 */
	public void addService(Service s) {
		servicios.add( s);
	}
	@Override
	public int compareTo(Taxi o) {
		if(taxi_id.compareTo(o.getTaxiId())<0)
			return -1;
		else if(taxi_id.compareTo(o.getTaxiId())>0)
			return 1;
		return 0;
	}
	public DoubleLinkedList<Service> darServicios()
	{
		return servicios;
	}
	public boolean serviceExists(Service s) {
		return false;
	}
}
