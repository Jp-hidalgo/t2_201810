package model.vo;

import java.util.Iterator;

/**
 * Representation of a Service object
 */
public class Service  implements Comparable<Service> ,Iterable<Service>{

	private String trip_id;
	private String taxi_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	private int endArea;
	/**
	 * carga un servicio a un taxi
	 * @param pTrip_id
	 * @param pTaxi_id
	 * @param pTrip_seconds
	 * @param pTrip_miles
	 * @param pTrip_total
	 */
	public Service(String pTrip_id,String pTaxi_id,int pTrip_seconds, double pTrip_miles, double pTrip_total,int pEndArea)
	{
		trip_id = pTrip_id;
		taxi_id = pTaxi_id;
		trip_seconds = pTrip_seconds;
		trip_miles = pTrip_miles;
		trip_total = pTrip_total;
		endArea = pEndArea;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return trip_total;
	}

	public int getEndComunityArea()
	{
		return endArea;
	}
	@Override
	public int compareTo(Service o) {
		return trip_seconds;
	}
	@Override
	public Iterator<Service> iterator() {
		return iterator();
	}
}
