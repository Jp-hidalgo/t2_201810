package model.data_structures;

/**
 * Created by zejian on 2016/10/23.
 */
public class Node<T> {

    public T data;
    public Node<T> prev, next;

    public Node(T data, Node<T> prev, Node<T> next)
    {
        this.data = data;
        this.prev = prev;
        this.next = next;
    }

    public Node(T data)
    {
        this(data, null, null);
    }

    public Node()
    {
        this(null, null, null);
    }

    public String toString()
    {
        return this.data.toString();
    }


}
