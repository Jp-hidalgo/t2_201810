package model.data_structures;

import java.util.Iterator;

import model.vo.Service;
import model.vo.Taxi;

public class DoubleLinkedList<T> implements LinkedList{

	protected Node<T> head; 
	protected Node<T> tail; 
	protected Node<T> current;
	private int currentpos;

	public DoubleLinkedList(){
		this.head =this.tail=this.current= new Node<>();
		currentpos = 0;
	}


	/**
	 * @param array
	 */
	public DoubleLinkedList(T[] array)
	{
		this();
		if (array!=null && array.length>0)
		{
			this.head.next = new Node<T>(array[0]);
			tail =this.head.next;
			tail.prev=this.head;
			int i=1;
			while (i<array.length)
			{
				tail.next=new Node<T>(array[i++]);
				tail.next.prev=tail;
				tail = tail.next;
			}
		}
	}

	@Override
	public boolean isEmpty() {
		return head.next==null;
	}


	@Override
	public int size() {
		int length=0;
		Node<T> pre=head.next;
		while (pre!=null){
			length++;
			pre=pre.next;
		}
		return length;
	}
	/**
	 * @param index
	 * @param data
	 * @return
	 */
	@Override
	public boolean add(int index, T data) {
		if(index<0||data==null)
			throw new NullPointerException("index < 0 || data == null");

		int j = 0;
		Node<T> front = this.head;
		while (front.next != null && j < index) {
			j++;
			front = front.next;
		}

		Node<T> q = new Node<T>(data, front, front.next);

		if(front.next != null) {
			front.next.prev = q;
		}
		front.next = q;
		if(front==this.tail){
			this.tail=q;
		}

		return true;
	}
	public void add(T data)
	{
		if(data==null)
			throw new NullPointerException("data == null");
		if(head.next == tail) {
			Node <T> newNode = new Node<T>(data,head,tail);
			head.next = newNode;
		}
		else
		{
			Node<T> newNode = new Node<T>(data,tail.prev,tail);
			tail.prev.next = newNode;
		}
	}

	/**
	 * @return
	 */
	@Override
	public T pop(int index) {

		int size=size();
		T temp=null;

		if(index<0||index>=size||isEmpty()){
			return temp;
		}

		Node<T> p=this.head;
		int j=0;
		while (p!=null&&j<=index){
			p=p.next;
			j++;
		}
		if(p.next!=null){
			p.next.prev=p.prev;
		}
		p.prev.next=p.next;
		if (p==this.tail) {
			this.tail = p.prev;
		}
		temp=p.data;

		return temp;
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	@Override
	public boolean removeAll(T data) {

		boolean isRemove=false;

		if(data==null||isEmpty())
			return isRemove;

		Node<T> p=this.head.next;

		while (p!=null){

			if(data.equals(p.data)){
				if (p==this.tail){
					this.tail=p.prev;
					p.prev=null;
					this.tail.next=null;
				}else {
					p.prev.next=p.next;
					p.next.prev=p.prev;
				}
				isRemove=true;
				p=p.next;
			}else {
				p=p.next;
			}

		}
		return isRemove;
	}

	/**
	 * clears the head and tail
	 */
	@Override
	public void clear() {
		this.head.next=null;
		this.tail=this.head;
	}
	@Override
	public String toString() {
		String str="(";
		Node<T> pre = this.head.next;
		while (pre!=null)
		{
			str += pre.data;
			pre = pre.next;
			if (pre!=null)
				str += ", ";
		}
		return str+")";
	}
	@Override
	public void remove() {
		// TODO Auto-generated method stub

	}


	@Override
	public boolean IsEmpty() {
		
		return head==tail;
	}


	@Override
	public T get(T n) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T getCurrent() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T next() {
		
		return null;
	}
	@Override
	public T get(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

}