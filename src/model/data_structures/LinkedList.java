package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the first element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Iterable<T>& Comparable<T>>  
{
	public void remove();
	public boolean IsEmpty();
	public T get(T n);
	public int size();
	public T get(int pos);
	public String toString();
	public T getCurrent();
	public T next();
	public void clear();
	public boolean add(int index, T data);
	public boolean isEmpty();
	public T pop(int index);
	boolean removeAll(T data);
}
