package model.logic;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.DoubleLinkedList;
import model.data_structures.LinkedList;

public class TaxiTripsManager implements ITaxiTripsManager {

	private DoubleLinkedList<Taxi> taxis;
	public void loadServices (String serviceFile) {
		File file = new File(serviceFile);
		JSONParser parser = new JSONParser();
		taxis = new DoubleLinkedList<Taxi>();
		try
		{
			JSONArray listaGson = (JSONArray) parser.parse(new FileReader(file));
			Iterator iter = listaGson.iterator();
			int z = 0;
			while(iter.hasNext())
			{
				JSONObject objetoJ= (JSONObject) listaGson.get(z);
				String company = objetoJ.get("company").toString();
				String dropoff_census_tract = objetoJ.get("dropoff_census_tract").toString();
				String dropoff_centroid_latitude = objetoJ.get("dropoff_centroid_latitude").toString();
				String dropoff_centroid_location = objetoJ.get("dropoff_centroid_location").toString();
				String dropoff_centroid_longitude = objetoJ.get("dropoff_centroid_longitude").toString();
				String dropoff_community_area = objetoJ.get("dropoff_community_area").toString();
				String extras = objetoJ.get("extras").toString();
				String fare = objetoJ.get("fare").toString();
				String payment_type = objetoJ.get("payment_type").toString();
				String pickup_census_tract = objetoJ.get("pickup_census_tract").toString();
				String pickup_centroid_latitude = objetoJ.get("pickup_centroid_latitude").toString();
				String pickup_centroid_location = objetoJ.get("pickup_centroid_location").toString();
				String pickup_centroid_longitude = objetoJ.get("pickup_centroid_longitude").toString();
				String pickup_community_area = objetoJ.get("pickup_community_area").toString();
				String taxi_id = objetoJ.get("taxi_id").toString();
				String tips = objetoJ.get("tips").toString();
				String tolls = objetoJ.get("tolls").toString();
				String trip_end_timestamp = objetoJ.get("trip_end_timestamp").toString();
				String trip_id = objetoJ.get("trip_id").toString();
				String trip_miles = objetoJ.get("trip_miles").toString();
				String trip_seconds = objetoJ.get("trip_seconds").toString();
				String trip_start_timestamp = objetoJ.get("trip_start_timestamp").toString();
				String trip_total = objetoJ.get("trip_total").toString();
				//TODO poner los datos que faltan
				Taxi newtaxi = new Taxi(taxi_id,company);
				int trips = Integer.parseInt(trip_seconds);
				double tripm = Double.parseDouble(trip_miles);
				double tript = Double.parseDouble(trip_total);
				int endArea = Integer.parseInt(dropoff_community_area);
				Service newService = new Service(trip_id,taxi_id,trips,tripm,tript,endArea);
				if(!taxiExists(newtaxi))
					taxis.add(newtaxi);
				if(!serviceExists(newService,newtaxi))
					newtaxi.addService(newService);
				iter.next();
				z++;
			}		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Inside loadServices with " + serviceFile);
		System.out.println("loaded "+taxis.size()+" taxis");
	}

	@Override
	public DoubleLinkedList<Taxi> getTaxisOfCompany(String company) {
		DoubleLinkedList<Taxi> res = new DoubleLinkedList<Taxi>();
		Iterator<Taxi> iter = taxis.iterator();
		while(iter.hasNext())
		{
			Taxi actual = iter.next();
			if(actual .getCompany().equals(company))
			{
				res.add(actual);
			}
		}
		System.out.println("Inside getTaxisOfCompany with " + company);
		return res;
	}

	@Override
	public DoubleLinkedList<Taxi> getTaxiServicesToCommunityArea(int communityArea) {
		boolean taxiAdded=false;
		DoubleLinkedList<Taxi> res = new DoubleLinkedList<Taxi>();
		Iterator<Taxi> iter = taxis.iterator();
		while(iter.hasNext())
		{
			Taxi actual = iter.next();
			taxiAdded = false;
			Iterator<Service> iter1 = actual.darServicios().iterator();
			while(iter1.hasNext()&&!taxiAdded)
			{
				Service actuals = iter1.next();
				if(actuals.getEndComunityArea()==(communityArea))
				{
					res.add(actual);
					taxiAdded =true;
				}
			}
			
		}
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return res;
	}
	public boolean taxiExists(Taxi t)
	{
		return false;
	}
	public boolean serviceExists(Service s,Taxi t)
	{
		return t.serviceExists(s);
	}
}
